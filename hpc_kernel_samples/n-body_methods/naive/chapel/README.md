# README - MPI and OpenMP (Naive n-Body Example)

## Description

This sample is a simple particle simulation in Chapel. [Chapel](http://chapel.cray.com/) is a parallel programming language developed by Cray. The code is explicitly not optmized for better understanding. This example shows how Chapel can be used to parallelize locally and over multiple compute nodes ("locales").

This code sample demonstates:
 * How to define Data structures and the corresponding domain mapping in Chapel
 * Data parallelism with the `forall` loop.
 
 The code sample is structured as followed:
 
  * `main.cpp`: The main program.
  * `Particle.hpp`: Particle structure.
  * `Vec3.hpp`: 3D-Vector structure, arithmetrical operators
  
  Futher readings:
  
  * Chapel [Quick Reference](http://chapel.cray.com/docs/latest/_downloads/quickReference.pdf)
  * Chapel [Language Specification](http://chapel.cray.com/docs/latest/_downloads/chapelLanguageSpec.pdf)
  * Chapel [Online Documentation](http://chapel.cray.com/docs/latest/)
  
## Release Date

2016-09-16


## Version History

 * 2016-09-16 Initial Release on PRACE CodeVault repository


## Contributors

 * Thomas Steinreiter - [thomas.steinreiter@risc-software.at](mailto:thomas.steinreiter@risc-software.at)


## Copyright

This code is available under Apache License, Version 2.0 - see also the license file in the CodeVault root directory.


## Languages

This sample is written in Cray Chapel.


## Parallelisation

This sample uses Chapels 'forall' loop for parallelisation.


## Level of the code sample complexity

Intermediate / Advanced


## Compiling

For compiling, you need the Chapel tools to be installed. Download: http://chapel.cray.com/download.html and follow the installation [instructions](http://chapel.cray.com/install.html#source).

Then run:

	chpl --optimize --specialize -o 4_nbody_naive_chapel main.chpl
	
Note: Chapel has its own launching mechansim, so you don't need `mpiexec`.

## Running

To run the program, use something similar to
	
	./4_nbody_naive_chapel --noIterations [ni] --noParticles [np]

either on the command line or in your batch script, where `ni` specifies the number of iterations and `np` specify the number of particles. If missing, the value `1000` is used.


### Command line arguments

 * `--noIterations [iterations]`: specify the number of iterations, which are simulated
 * `--noParticles [particles]`: specify the number of particles in the simulation
 
### Example
 
If you run
	
	mpiexec -n 4 ./4_nbody_naive_chapel --noParticles 1000 --noIterations 1000

the output should look similar to

	Initialization done.
	0X: 0.305652
	1X: -0.409045
	2X: 0.0428421
	3X: 0.630893
	4X: -0.200929
	5X: -0.320789
	6X: -0.457138
	7X: -0.445587
	8X: 0.117244
	9X: 0.927159
	End Simulation
	0X: 0.317129
	1X: -0.415535
	2X: 0.0497131
	3X: 0.64254
	4X: -0.182432
	5X: -0.3343
	6X: -0.467756
	7X: -0.456107
	8X: 0.127922
	9X: 0.94022
	Execution time:5.4289
