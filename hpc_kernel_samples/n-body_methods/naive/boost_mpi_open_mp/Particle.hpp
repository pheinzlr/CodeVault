#pragma once

#include "Vec3.hpp"

#include <boost/serialization/serialization.hpp>

struct Particle {
	double Mass{0};
	Vec3 Location;
	Vec3 Velocity;

	template <class Archive> void serialize(Archive& ar, const unsigned int version) {
		ar& Mass;
		ar& Location;
		ar& Velocity;
	}

  private:
	friend class boost::serialization::access;
};
BOOST_IS_MPI_DATATYPE(Particle) // performance hint allowed for contiguous data
