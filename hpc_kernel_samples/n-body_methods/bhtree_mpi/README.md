=======
README
=======

# 1. Code sample name
4_nbody_bhtree_mpi

# 2. Description of the code sample package
This example demonstrates an MPI  implementation of the Barnes-Hut nbody algorithm.

Additional pre-requisites:
* MPI

# 3. Release date
28 October 2016

# 4. Version history 
1.0

# 5. Contributor (s) / Maintainer(s) 
Paul Heinzlreiter <paul.heinzlreiter@risc-software.at>

# 6. Copyright / License of the code sample
Apache 2.0

# 7. Language(s) 
C++

# 8. Parallelisation Implementation(s)
multi-core CPU and / or multiple CPUs/machines

# 9. Level of the code sample complexity 
Source-level implementation

# 10. Instructions on how to compile the code
cmake <bhtree_mpi directory>
make

# 11. Instructions on how to run the code
mpirun -np <number of processes> 4_nbody_bhtree_mpi <bhtree_mpi directory>/src/data/<input_file>

# 12. Sample input(s)
Sample input data files are provided in <bhtree_mpi directory>/src/data
They are partially taken from
http://bima.astro.umd.edu/nemo/archive/#dubinski

# 13. Sample output(s)

