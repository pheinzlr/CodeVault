#ifndef MPI_SIMULATION_HPP
#define MPI_SIMULATION_HPP


#include <mpi.h>
#include <Simulation.hpp>
#include <BarnesHutTree.hpp>
#include <cstddef>
#include <vector>

namespace nbody {
	using namespace std;

	typedef struct SendStoreStruct {
		Body* bodies;
		MPI_Request request;
		int size;
	} SendStore;

	//MPI simulation
	class MpiSimulation : public Simulation {
	protected:
		MPI_Datatype bodyType;
		MPI_Datatype boxType;
		vector<Box> domains;
		Box overallDomain;
		vector<SendStore> sendStores;

		virtual SendStore* availableSendStore(int numElems);
		virtual void send(vector<Body> bodies, int target);
		virtual int recv(vector<Body>& bodies, int source);
	public:
		MpiSimulation();
		virtual ~MpiSimulation();
		virtual void initialize(string inputFile);
		virtual void cleanup();
		virtual int getNumberOfProcesses();
		virtual int getProcessId();
		virtual bool stateCorrect();
		virtual void distributeBodies();
		virtual void distributeDomains(vector<Body> localBodies);
		virtual void distributeDomains(Box localDomain);
		virtual void distributeDomains();
		virtual void distributeLETs();
		virtual void rebuildTree();
		virtual void buildTree();
		virtual void runStep();
		virtual MPI_Datatype* getDatatype();
	};
}

#endif

