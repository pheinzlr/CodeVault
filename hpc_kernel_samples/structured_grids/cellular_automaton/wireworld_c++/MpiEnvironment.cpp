#include "MpiEnvironment.hpp"

#include <utility>

void MpiEnvironment::swap(MpiEnvironment& first,
                          MpiEnvironment& second) noexcept {
	using std::swap;
	swap(first._worldRank, second._worldRank);
	swap(first._worldSize, second._worldSize);
	swap(first._isMaster, second._isMaster);
}

MpiEnvironment::MpiEnvironment(int& argc, char* argv[]) {
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &_worldRank);
	MPI_Comm_size(MPI_COMM_WORLD, &_worldSize);
	_isMaster = {_worldRank == 0};
	// We want the program to stop on I/O errors
	MPI_File_set_errhandler(MPI_FILE_NULL, MPI_ERRORS_ARE_FATAL);
}
MpiEnvironment::~MpiEnvironment() {
	if (_worldRank != -1) { MPI_Finalize(); }
}

MpiEnvironment::MpiEnvironment(MpiEnvironment&& other) noexcept {
	swap(*this, other);
}
MpiEnvironment& MpiEnvironment::operator=(MpiEnvironment&& other) noexcept {
	swap(*this, other);
	return *this;
}
