#pragma once

#include <cstddef>
#include <ostream>

#include "Communicator.hpp"
#include "MpiEnvironment.hpp"
#include "Tile.hpp"

class MpiWireworld {
	const MpiEnvironment& _env;
	const Configuration& _cfg;
	Tile _tile;
	Communicator _comm;

	void processArea(Coord start, Size size);

  public:
	MpiWireworld(const MpiEnvironment& env, const Configuration& cfg);
	friend std::ostream& operator<<(std::ostream& out, const MpiWireworld& g);
	void write() const;
	void simulateStep();
};