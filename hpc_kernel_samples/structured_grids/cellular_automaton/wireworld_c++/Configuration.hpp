#pragma once

#include <cstddef>
#include <string>

#include "MpiEnvironment.hpp"
#include "Util.hpp"

enum class CommunicationMode {
	Collective, //
	P2P         //
};

struct Configuration {
	Size Procs{};
	std::string InputFilePath;
	std::string OutputFilePath;
	std::size_t Generations{1000};
	CommunicationMode CommMode{CommunicationMode::Collective};

	static auto parseArgs(int argc, char* argv[], const MpiEnvironment& env)
	    -> Configuration;
};
