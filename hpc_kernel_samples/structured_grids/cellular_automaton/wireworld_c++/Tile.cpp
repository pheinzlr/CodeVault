#include "Tile.hpp"

#include <iostream>
#include <iterator>
#include <string>

#include <mpi.h>

#include "FileIO.hpp"
#include "State.hpp"

Tile::Tile(const Configuration& cfg, const MpiEnvironment& env)
    : _env(env), _cfg(cfg), _header(FileIO::ReadHeader(cfg.InputFilePath)), //
      _tileSize(FileIO::GetTileInfo(_header.GlobalSize, cfg.Procs, env.worldRank()).Size),
      _modelWidth(_tileSize.Cols + 2) {
	const auto bufsize = (_tileSize.Cols + 2) * (_tileSize.Rows + 2);
	_memoryA.resize(bufsize);
	_memoryB.resize(bufsize);

	_model = _memoryA.data();
	_nextModel = _memoryB.data();

	FileIO::Tile(cfg.InputFilePath, _header, cfg.Procs, _env.worldRank(), _model)
	    .Read();
}

Tile Tile::Read(const Configuration& cfg, const MpiEnvironment& env) {
	return {cfg, env};
}

void Tile::write() const {
	const auto& path = _cfg.OutputFilePath;
	FileIO::WriteHeader(_header, path, _env);
	FileIO::Tile(path, _header, _cfg.Procs, _env.worldRank(), _model).Write();
}

std::ostream& operator<<(std::ostream& out, const Tile& t) {
	const auto hline = [](auto& out, auto length) {
		out << '+';
		std::fill_n(std::ostream_iterator<char>(out), length, '-');
		out << "+\n";
	};
	hline(out, t.tileSize().Cols);
	for (std::size_t y{1}; y <= t.tileSize().Rows; ++y) {
		out << '|';
		for (std::size_t x{1}; x <= t.tileSize().Cols; ++x) {
			out << to_integral(t.model()[y * t.modelWidth() + x]);
		}
		out << "|\n";
	}
	hline(out, t.tileSize().Cols);
	return out;
}