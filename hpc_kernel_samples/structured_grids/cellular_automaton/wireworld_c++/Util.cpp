#include "Util.hpp"

#include <mpi.h>

#include <iostream>

void MpiReportErrorAbort(const std::string& err) {
	std::cerr << err << '\n';
	MPI_Abort(MPI_COMM_WORLD, EXIT_FAILURE);
	std::abort(); // workaround for compiler warnings
}