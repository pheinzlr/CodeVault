#pragma once
#include <cstddef>
#include <initializer_list>
#include <mpi.h>
#include <vector>

struct SubarrayDimensionDefinition { // helper container
	std::size_t size{};
	std::size_t subSize{};
	std::size_t start{};
};

class SubarrayDefinition { // helper container for MPI Datatype creation
	std::vector<int> _sizes;
	std::vector<int> _subSizes;
	std::vector<int> _starts;

  public:
	int dims() const { return _sizes.size(); }
	int* sizes() { return _sizes.data(); }
	int* subSizes() { return _subSizes.data(); }
	int* starts() { return _starts.data(); }

	SubarrayDefinition(
	    std::initializer_list<SubarrayDimensionDefinition> saDimDefs);
};

class MpiSubarray { // wrapper for creating and destroying the type
	MPI_Datatype _type{MPI_DATATYPE_NULL};

  public:
	void swap(MpiSubarray& first, MpiSubarray& second) noexcept;
	MPI_Datatype type() const { return _type; }

	MpiSubarray(SubarrayDefinition sd);
	~MpiSubarray();
	MpiSubarray(MpiSubarray&) = delete;
	MpiSubarray& operator=(MpiSubarray&) = delete;
	MpiSubarray(MpiSubarray&& other) noexcept;
	MpiSubarray& operator=(MpiSubarray&& other) noexcept;
};
