#include "petsc.h"
#include "petscfix.h"
/* damgsnes.c */
/* Fortran interface file */

/*
* This file was generated automatically by bfort from the C source
* file.  
 */

#ifdef PETSC_USE_POINTER_CONVERSION
#if defined(__cplusplus)
extern "C" { 
#endif 
extern void *PetscToPointer(void*);
extern int PetscFromPointer(void *);
extern void PetscRmPointer(void*);
#if defined(__cplusplus)
} 
#endif 

#else

#define PetscToPointer(a) (*(long *)(a))
#define PetscFromPointer(a) (long)(a)
#define PetscRmPointer(a)
#endif

#include "petscda.h"
#include "petscmg.h"
#include "petscdmmg.h"
#include "petscmat.h"
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define dmmgsetsneslocalfd_ DMMGSETSNESLOCALFD
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define dmmgsetsneslocalfd_ dmmgsetsneslocalfd
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define dmmgsetsneslocal_ DMMGSETSNESLOCAL
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define dmmgsetsneslocal_ dmmgsetsneslocal
#endif


/* Definitions of Fortran Wrapper routines */
#if defined(__cplusplus)
extern "C" {
#endif
void PETSC_STDCALL  dmmgsetsneslocalfd_(DMMG *dmmg,DALocalFunction1 *function, int *__ierr ){
*__ierr = DMMGSetSNESLocalFD(dmmg,*function);
}
void PETSC_STDCALL  dmmgsetsneslocal_(DMMG *dmmg,DALocalFunction1 *function,DALocalFunction1 *jacobian,
                        DALocalFunction1 *ad_function,DALocalFunction1 *admf_function, int *__ierr ){
*__ierr = DMMGSetSNESLocal(dmmg,*function,*jacobian,*ad_function,*admf_function);
}
#if defined(__cplusplus)
}
#endif
